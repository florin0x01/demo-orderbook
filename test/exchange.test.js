const {Exchange, Entry} = require('../lib/index')
const assert = require('assert')
const process = require('process')

function test_matches_no_partials() {
    e = new Exchange()
    e.buy(10, 2)
    e.buy(5, 3)
    e.sell(50, 8)
    e.sell(1, 9)
    e.sell(5, 12)
    e.buy(52, 9)
    e.sell(1, 9)

    assert.equal(e._sell_orders.length, 1)
    assert.equal(e._buy_orders.length, 2)
    assert.equal(e._buy_orders[0].price, 3)
    assert.equal(e._buy_orders[0].quantity, 5)
    assert.equal(e._buy_orders[0].executedQuantity, 0)
    assert.equal(e._buy_orders[0].isBuyOrder, true)
    assert.equal(e._buy_orders[1].isBuyOrder, true)
    assert.equal(e._sell_orders[0].price, 12)
    assert.equal(e._sell_orders[0].quantity, 5)
    assert.equal(e._sell_orders[0].executedQuantity, 0)
    assert.equal(Object.keys(e._archived_orders).length, 4)
}

function test_matches_partials() {
    e = new Exchange()
    e.buy(10, 2)
    e.buy(5, 3)
    e.sell(50, 8)
    e.sell(1, 9)
    e.sell(5, 12)
    e.buy(52, 9)

    assert.equal(e._sell_orders.length, 1)
    assert.equal(e._buy_orders.length, 3)
    assert.equal(e._buy_orders[0].price, 9)
    assert.equal(e._buy_orders[0].quantity, 52)
    assert.equal(e._buy_orders[0].executedQuantity, 51)
    assert.equal(e._buy_orders[1].price, 3)
    assert.equal(e._buy_orders[1].quantity, 5)
    assert.equal(e._buy_orders[1].executedQuantity, 0)
    assert.equal(e._buy_orders[2].price, 2)
    assert.equal(e._buy_orders[2].quantity, 10)
    assert.equal(e._buy_orders[2].executedQuantity, 0)

    assert.equal(e._buy_orders[0].isBuyOrder, true)
    assert.equal(e._buy_orders[1].isBuyOrder, true)
    assert.equal(e._buy_orders[2].isBuyOrder, true)

    assert.equal(e._sell_orders[0].price, 12)
    assert.equal(e._sell_orders[0].quantity, 5)
    assert.equal(e._sell_orders[0].executedQuantity, 0)
    assert.equal(e._sell_orders[0].isBuyOrder, false)
    assert.equal(Object.keys(e._archived_orders).length, 2)
}

function test_no_matches() {
    e = new Exchange()
    o1 = e.buy(10, 2)
    o2 = e.buy(5, 3)

    o3 = e.sell(10, 20)
    o4 = e.sell(4, 5)
    assert.equal(e._buy_orders.length, 2)
    assert.equal(e._sell_orders.length, 2)
    assert.equal(e._buy_orders[0].price, 3)
    assert.equal(e._buy_orders[0].executedQuantity, 0)
    assert.equal(e._buy_orders[0].isBuyOrder, true)
    assert.equal(e._buy_orders[1].price, 2)
    assert.equal(e._buy_orders[1].quantity, 10)
    assert.equal(e._buy_orders[1].executedQuantity, 0)
    assert.equal(e._buy_orders[1].isBuyOrder, true)
    assert.equal(e._sell_orders[0].price, 5)
    assert.equal(e._sell_orders[0].quantity, 4)
    assert.equal(e._sell_orders[0].executedQuantity, 0)
    assert.equal(e._sell_orders[0].isBuyOrder, false)
    assert.equal(e._sell_orders[1].price, 20)
    assert.equal(e._sell_orders[1].quantity, 10)
    assert.equal(e._sell_orders[1].executedQuantity, 0)
    assert.equal(e._sell_orders[1].isBuyOrder, false)

    assert.equal(o1.id, e.getOrder(o1.id).id)
    assert.equal(o2.id, e.getOrder(o2.id).id)
    assert.equal(o3.id, e.getOrder(o3.id).id)
    assert.equal(o4.id, e.getOrder(o4.id).id)

    assert.equal(Object.keys(e._archived_orders).length, 0)
}

function test_full_matches() {
    e = new Exchange()
    o1 = e.buy(10, 2)
    o2 = e.sell(10, 1)
    assert.equal(Object.keys(e._archived_orders).length, 2)
    assert.equal(o1.id, e.getOrder(o1.id).id)
    assert.equal(o2.id, e.getOrder(o2.id).id)
}

function test_get_quantity_at_price_1() {
    e = new Exchange()
    e.buy(10, 2)
    e.buy(15, 2)
    e.buy(20, 3)
    assert.equal(e.getQuantityAtPrice(2), 25)
    assert.equal(e.getQuantityAtPrice(3), 20)

    e.sell(7, 2)
    e.sell(13, 2)
    assert.equal(e.getQuantityAtPrice(2), 25)
}

function test_get_quantity_at_price_2() {
    e = new Exchange()
    e.buy(10, 2)
    e.buy(15, 2)

    e.sell(7, 2)
    assert.equal(e.getQuantityAtPrice(2), 25)
}


function test_get_quantity_at_price_3() {
    e = new Exchange()
    e.sell(10, 2)
    e.sell(11, 2)

    e.buy(7, 2)
    assert.equal(e.getQuantityAtPrice(2), 21)
}

function test_sync_fail() {
    e = new Exchange()
    let status = e.sync(process.cwd() + "/test/fail.js")
    assert.equal(status, false)
}

function test_sync_ok() {
    e = new Exchange()
    let status = e.sync(process.cwd() + "/test/ok.js")
    assert.equal(status, true)
    assert.equal(Object.keys(e._archived_orders).length, 1)
    assert.equal(e._sell_orders.length, 2)
    assert.equal(e._sell_orders[0].price, 2)
}

test_full_matches()
test_no_matches()
test_matches_partials()
test_matches_no_partials()
test_get_quantity_at_price_1()
test_get_quantity_at_price_2()
test_get_quantity_at_price_3()
test_sync_fail()
test_sync_ok()
