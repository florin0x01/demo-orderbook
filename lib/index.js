const fs = require('fs')

class Entry {
    constructor(price, quantity) {
        this.price = price;
        this.quantity = quantity
        this.timestamp = Date.now()
        this.executedQuantity = 0
        this.isBuyOrder = false
    }
}

class Exchange {
    constructor() {
        this._buy_orders = []
        this._sell_orders = []
        this._archived_orders = {}
        this._cur_id = 0
    }

    _getNextId() {
        return ++this._cur_id
    }

    _serialize() {
        return JSON.stringify({
            _buy_orders: this._buy_orders,
            _sell_orders: this._sell_orders,
            _archived_orders: this._archived_orders,
            _cur_id: this._cur_id
        })
    }

    sync(fileName) {
        try {
            let contents = fs.readFileSync(fileName)
            contents = JSON.parse(contents.toString('utf8'))
            this._buy_orders = contents._buy_orders
            this._sell_orders = contents._sell_orders
            this._archived_orders = contents._archived_orders
            this._cur_id = contents._cur_id
            return true
        } catch (exc) {
            return false
        }
    }

    _add(arr, elem, sort_asc) {
        let sorter = null;

        if (!arr) {
            arr = [elem]
        } else {
            arr.push(elem)
        }
        if (sort_asc) {
            sorter = (el1, el2) => {
                if (el1.price === el2.price) {
                    return el1.timestamp < el2.timestamp
                }
                return el1.price > el2.price
            }
        } else {
            sorter = (el1, el2) => {
                if (el1.price === el2.price) {
                    return el1.timestamp < el2.timestamp
                }
                return el1.price < el2.price
            }
        }
        arr.sort(sorter)
    }

    _match(elem, other_arr, compar_fn_price) {
        for (let idx = 0; idx < other_arr.length;) {
            if (elem.executedQuantity === elem.quantity) {
                const {timestamp, ...rest} = elem
                this._archived_orders[elem.id] = rest
                return true
            }
            if (!compar_fn_price(elem, other_arr[idx])) {
                idx++
                continue
            }
            elem.executedQuantity += Math.min(other_arr[idx].quantity, elem.quantity)
            other_arr[idx].executedQuantity += Math.min(other_arr[idx].quantity, elem.quantity)
            if (other_arr[idx].executedQuantity === other_arr[idx].quantity) {
                const {timestamp, ...rest} = other_arr[idx]
                this._archived_orders[other_arr[idx].id] = rest
                other_arr.splice(idx, 1)
                continue
            }
            idx++
        }
        if (elem.executedQuantity === elem.quantity) {
            const {timestamp, ...rest} = elem
            this._archived_orders[elem.id] = rest
            return true
        }
        return false
    }

    _buy_sell_wrapper(isBuyOrder, quantity, price, other_array, reference_array, compar_fn_price) {
        let e = new Entry(price, quantity)
        e.id = this._getNextId()
        e.isBuyOrder = isBuyOrder
        if (this._match(e, other_array, compar_fn_price) === false) {
            this._add(reference_array, e, !isBuyOrder)
        }
        const {timestamp, ...rest} = e
        return rest
    }

    buy(quantity, price) {
        return this._buy_sell_wrapper(true, quantity, price, this._sell_orders, this._buy_orders, (elem, other_elem) => elem.price - other_elem.price >= 0)
    }

    sell(quantity, price) {
        return this._buy_sell_wrapper(false, quantity, price, this._buy_orders, this._sell_orders, (elem, other_elem) => other_elem.price - elem.price >= 0)
    }

    //returns index
    _bin_search(arr, price, comparator) {
        let start = 0, end = arr.length - 1
        while (start <= end) {
            let mid = Math.floor((start + end) / 2)
            if (arr[mid].price === price) {
                return mid
            }
            if (comparator(arr[mid], price)) {
                start = mid + 1
            } else {
                end = mid - 1
            }
        }
        return -1
    }

    _getQuantityAtPrice(arr, price, comparator) {
        let quantity = -1
        if (arr.length === 1) {
            if (arr[0].price === price) {
                return arr[0].quantity
            }
            return -1
        }
        let s_idx = this._bin_search(arr, price, comparator)
        if (s_idx === -1) {
            return -1
        }
        quantity = arr[s_idx].quantity
        for (let i = s_idx + 1; i < arr.length && arr[i].price === price; i++) {
            quantity += arr[i].quantity
        }
        for (let i = s_idx - 1; i >= 0 && arr[i].price === price; i--) {
            quantity += arr[i].quantity
        }
        return quantity
    }

    _getOrder(arr, id) {
        arr = arr.filter((val) => val.id === id)
        if (!arr || !arr.length) {
            return null
        }
        const {timestamp, ...rest} = arr[0]
        return rest
    }

    getQuantityAtPrice(price) {
        let quantity = this._getQuantityAtPrice(this._sell_orders, price, (a,price) => a.price < price)
        if (quantity === -1) {
            quantity = this._getQuantityAtPrice(this._buy_orders, price, (a,price) => a.price > price)
        }
        if (quantity === -1) {
            return 0
        }
        return quantity
    }

    getOrder(id) {
        if (id in this._archived_orders) {
            return this._archived_orders[id]
        }
        let order = this._getOrder(this._sell_orders, id)
        if (!order) {
            order = this._getOrder(this._buy_orders, id)
        }
        return order
    }
}

module.exports = { Exchange, Entry }
